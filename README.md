X²
===

Xsquared or X² is a project designed to update XOOPS v2.0.18 and rework it
to use major portions of symfony.

Usage of composer will be a major area to be thought about, too.

With composer we can get rid of a lot of code that was written or rewritten internally before.
So we would make it a require, override with what we need and that's it.

Languages can be ported to symfony, it is much easier to use. Multilingual sites are done out of the box then.
